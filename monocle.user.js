// ==UserScript==
// @name        Monocle
// @namespace   Glibertarians
// @include     *glibertarians.com/2*
// @version     2.02
// @grant       letMeIn
// @run-at      document-idle
// @require     http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js
// ==/UserScript==

//CONSTANTS AND OTHER GLOBALS
var hideOldThreadsStorageTag = "hideOldThreads";
var commentPermaLinkStorageTag = "commentLink";
var timeZoneStorageTag = "chgTimeZone";
var dynamicLoadStorageTag = "dynamicLoad";
var compactCommentStorageTag = "compactComment";
var unreadCount = 0;
var timer = 0;
var movedReply = 0;

//MAIN
$( "#back_to_top" ).remove();
initSettings();
doStyleSheet();
customFormatter();
handleComments();
toolbar();





function initSettings(){
  if ( typeof(localStorage.getItem(hideOldThreadsStorageTag)) == 'undefined' ) {
    localStorage.setItem(hideOldThreadsStorageTag, 0);
  }
  if ( typeof(localStorage.getItem(commentPermaLinkStorageTag)) == 'undefined'){
    localStorage.setItem(commentPermaLinkStorageTag, 0);
  }
  if ( typeof(localStorage.getItem(timeZoneStorageTag)) == 'undefined'){
      localStorage.setItem(timeZoneStorageTag, 0);
  }	
  if ( typeof(localStorage.getItem(dynamicLoadStorageTag)) == 'undefined'){
      localStorage.setItem(dynamicLoadStorageTag, 0);
  }		
  if ( typeof(localStorage.getItem(compactCommentStorageTag)) == 'undefined'){
      localStorage.setItem(compactCommentStorageTag, 0);
  }		
}

function doStyleSheet(){
    $("#monocle-style-css").remove();
    var styleEle = document.createElement('style');
    styleEle.type = 'text/css';
    styleEle.id = "monocle-style-css";
    styleEle.textContent = '#monocle_tb_container { position: fixed; z-index: 100; left: 0px; bottom: 0px; height: 60px; width: 100%; } ' +
        '#monocle_tb_container.mini { height: 30px; } ' +        
        '#monocle_tb_container input {font-size: 12px;}' + 
        '#monocle_toolbar { left:30px; bottom:0px; height: 30px; width:100%; background-color: rgba(0,0,0,0.5); } ' +
        '#monocle_tb_container.mini #monocle_toolbar { height: 0px; } ' +
        '#options_menu { position:fixed; z-index: 101; right:0px; bottom:30px; height: 400px; width:250px; background-color: rgba(0,0,0,0.5); } ' +
        '#monocle_tb_container.mini #options_menu { height: 0px; } ' +
        '#options_menu.mini { height: 0px; } ' +
	      '.commentlist {margin-bottom: 0px; } ' +
	      '.hidden {display: none;}' +
        '#shr-admin-badge {display: none;}' +
        '#options_menu > input.option_text { background-image: none; padding: 1px 1px 1px 1px; font-size: small; height: 25px; width: 232px; background-color: rgba(0,0,0,0); color: white}' +
        '#options_menu > input.option_text.selected {background-color: rgba(255,255,255,0.5)}' +     
        'input.mono_button { background-image: none; padding: 1px 1px 1px 1px; height: 30px; text-shadow: #ffffff 0px 0px 2px; -webkit-font-smoothing: antialiased } ' +
        '#monocle_toolbar > #unread_button { float:left; width: 160px; background-color: rgba(0,0,0,0.9); color: white; } ' +
        '#monocle_toolbar > #mark_read_button { float:left; width: 175px; background-color: rgba(0,0,0,0.9); color: white; } ' +
        '#monocle_toolbar > #to_top_button { float:left; width: 160px; background-color: rgba(0,0,0,0.9); color: white; } ' +
        '#monocle_toolbar > #to_bot_button { float:left; width: 160px; background-color: rgba(0,0,0,0.9); color: white; } ' +
        '#monocle_toolbar > #old_thread_button { float:left; width: 160px; background-color: rgba(0,0,0,0.9); color: white } ' +
        '#monocle_toolbar > #options_button { float:right; width: 160px; background-color: rgba(0,0,0,0.9); color: white } ' +  
        '#monocle_tb_container > #toggle_button { left:0px; bottom:0px; width:20px; background-color: rgba(0,0,0,0.5); }' +
        '#comment-wrap.onlyNew li.comment { display: none; } ' +
        '#comment-wrap.onlyNew li.comment.new-comment { display: block; } ' +
        '#comment-wrap.onlyNew li.comment.fresh-comment { display: block; } ' +
        '#comment-wrap.onlyNewGrat li.comment { display: none; } ' +
        '#comment-wrap.onlyNewGrat li.comment.new-comment { display: block; } ' +
        '#comment-wrap.onlyNewGrat li.comment.fresh-comment { display: block; } ' +
        '#comment-wrap.onlyNewGrat li.comment.fresh-grat-comment { display: block; } ' +
        ' li.comment.compact > article.comment-body { margin-bottom: 0px; } ' +
	' li.comment.deepest-comment.compact > article.comment-body { margin-bottom: 10px; } ';
    document.head.appendChild(styleEle);
}

function handleComments(){
  //Local TZ
  if (localStorage.getItem(timeZoneStorageTag) == 1){
    try{
      $( ".comment_date" ).each(function(){adjustTime(this);});
    }
    catch(error){}
  }
  //Comment Permalink
  if (localStorage.getItem(commentPermaLinkStorageTag) == 1){
    try{
      $( ".comment_postinfo" ).each(function(){addLink(this);});
    }
    catch(error){}
  }

  //Parse New Comments
  $(".new-comment" ).siblings().addClass("fresh-comment");
  $(".depth-1").removeClass("fresh-comment");
  $(".fresh-comment" ).find(".comment").addClass("fresh-grat-comment");
  $(".new-comment" ).parents(".comment").addClass("fresh-comment");
}

function adjustTime(commentDate){ 
  var dateTokens = commentDate.innerHTML.split(" ");
  var tzOffset = isDstActive() ? "GMT-0500" : "GMT-0600";
  var dateObj = new Date(dateTokens[1] + " " + dateTokens[2] + " " + dateTokens[3] + " " + dateTokens[5] + " " + dateTokens[6] + " " + tzOffset);
  var options = {
    year: "numeric", month: "long",
    day: "numeric", hour: "2-digit", minute: "2-digit"
  };
  commentDate.innerHTML = "on " + dateObj.toLocaleTimeString("en-us",options);
}

function isDstActive(){
  // based on stackoverflow.com/q/11887934
  // this is faulty because it relies on the user's DST transition
  // happening on the same day as the server's DST transition
  var now = new Date();
  var jan = new Date(now.getFullYear(), 0, 1);
  return jan.getTimezoneOffset() != now.getTimezoneOffset();
}

function addLink(commentHeader){
  var link = document.createElement("a");
  link.className = "comment_link";
  var ifrstr = "";
  var ifrurl = document.URL;
  ifrurl = ifrurl.split('/');
  for (var i = 3; i < ifrurl.length; i++){
    if (ifrurl[i].trim() == "" || ifrurl[i].startsWith("#")){
      break;
    }
    ifrstr = ifrstr + "/" + ifrurl[i];
  }
  link.href = ifrstr + "/#" + commentHeader.parentNode.id;
  link.innerHTML = "[+]";
  commentHeader.appendChild(link);
}

function toolbar(){
  var toolbar = document.createElement("div");
  toolbar.id = "monocle_tb_container";

  //visible portion
  var activebar = document.createElement("div");
  activebar.id = "monocle_toolbar";
  activebar.className = "visible";
  //scrolls to unread comments
  var nextUnreadButton = document.createElement("input");
  nextUnreadButton.id = "unread_button";
  nextUnreadButton.type = "button";
  nextUnreadButton.className = "mono_button";
  nextUnreadButton.value = document.querySelectorAll(".new-comment").length + " Unread Comments";
  if (document.querySelectorAll(".new-comment").length == 1){
    nextUnreadButton.value = document.querySelectorAll(".new-comment").length + " Unread Comment";
  }
  nextUnreadButton.addEventListener("click",seekNextUnread);
  activebar.appendChild(nextUnreadButton);
  //toggles visibility of old threads
  var oldThreadButton = document.createElement("input");
  oldThreadButton.id = "old_thread_button";
  oldThreadButton.type = "button";
  oldThreadButton.className = "mono_button";
  oldThreadButton.value="Hide Old Threads";
  oldThreadButton.addEventListener("click", toggleOldThreads);
  activebar.appendChild(oldThreadButton);
  //moves view to top
  var toTopButton = document.createElement("input");
  toTopButton.id = "to_top_button";
  toTopButton.type = "button";
  toTopButton.className = "mono_button";
  toTopButton.value="Top of Comments";
  toTopButton.addEventListener("click", navigateToTop);
  activebar.appendChild(toTopButton);
  //moves view to bottom
  var toBotButton = document.createElement("input");
  toBotButton.id = "to_bot_button";
  toBotButton.type = "button";
  toBotButton.className = "mono_button";
  toBotButton.value="Bottom of Comments";
  toBotButton.addEventListener("click", navigateToBottom);
  activebar.appendChild(toBotButton);
  //clears new comments
  var markReadButton = document.createElement("input");
  markReadButton.id = "mark_read_button";
  markReadButton.type = "button";
  markReadButton.className = "mono_button";
  markReadButton.value="Mark Comments as Read";
  markReadButton.addEventListener("click", markAsRead);
  activebar.appendChild(markReadButton);
  //toggles visibility of toolbar
  var toggleButton = document.createElement("input");
  toggleButton.id = "toggle_button";
  toggleButton.type = "button";
  toggleButton.className = "mono_button";
  toggleButton.value = "v";
  toggleButton.addEventListener("click",toggleToolbar);
  toolbar.appendChild(toggleButton);
  toolbar.appendChild(activebar);
  //document.getElementById("footer").appendChild(toolbar);
  document.getElementsByClassName("et-l--footer")[0].appendChild(toolbar);
  setShowThreads(oldThreadButton); //sets button value
  //create options button and bar
  createOptionsBar();
  var optionsButton = document.createElement('input');
  optionsButton.id = 'options_button';
  optionsButton.type ='button';
  optionsButton.className = 'mono_button';
  optionsButton.value = 'Options';  
  optionsButton.addEventListener('click', toggleOptionsBar);
  activebar.appendChild(optionsButton);
}

function seekNextUnread(){
  var unreadList = document.querySelectorAll(".new-comment");
  if (unreadCount >= unreadList.length){
    unreadCount = 0;
  }
  if (unreadList.length > 0){
   unreadList[unreadCount].scrollIntoView();
   window.scrollBy(0,-50);
  }
  unreadCount+=1;
}

function toggleOldThreads(){
  if (localStorage.getItem(hideOldThreadsStorageTag) == 1){
    localStorage.setItem(hideOldThreadsStorageTag, 2);
  }
  else if (localStorage.getItem(hideOldThreadsStorageTag) == 2){
    localStorage.setItem(hideOldThreadsStorageTag, 0);
  }
  else{
    localStorage.setItem(hideOldThreadsStorageTag, 1);
  }
  setShowThreads(this);
}

function setShowThreads(thisEle) {
  if (localStorage.getItem(hideOldThreadsStorageTag) == 1){
    thisEle.value = "Hide Less Threads";
    $("#comment-wrap").addClass("onlyNew");
  } 
  else if (localStorage.getItem(hideOldThreadsStorageTag) == 2){
    thisEle.value = "Show Old Threads";
    $("#comment-wrap").removeClass("onlyNew");
    $("#comment-wrap").addClass("onlyNewGrat");
  }
  else{
    thisEle.value = "Hide Old Threads";
    $("#comment-wrap").removeClass("onlyNewGrat");
  }
}

function navigateToTop(){
  document.getElementById("comments").scrollIntoView();
  window.scrollBy(0,-50);
}

function navigateToBottom(){
  document.getElementById("footer").scrollIntoView();
  window.scrollBy(0,-500);
}

function markAsRead(){
  $( ".new-comment" ).removeClass("new-comment");
  $( ".fresh-comment" ).removeClass("fresh-comment");
  $( ".fresh-grat-comment" ).removeClass("fresh-grat-comment");
  $( "#unread_button" ).val("0 Unread Comments");
}

function toggleToolbar(){
  if ( document.getElementById("monocle_tb_container").classList.contains("mini") ) {
    document.getElementById("monocle_tb_container").classList.remove("mini");
    document.getElementById("respond").classList.remove("bottom");
    document.getElementById("toggle_button").value = "v";
  }
  else{
    document.getElementById("monocle_tb_container").classList.add("mini");
    document.getElementById("respond").classList.add("bottom");
    document.getElementById("toggle_button").value = "^";
  }
}

function createOptionsBar(){
  var toolbar = document.getElementById('monocle_tb_container');
  var visibar = document.getElementById('monocle_toolbar');
  var optionsbar = document.createElement('div');
  optionsbar.id = "options_menu";  
  optionsbar.style = "overflow-y:scroll";
  optionsbar.classList.add("mini");
  document.body.appendChild(optionsbar);
  //comment links - fomerlyexpand column
  var expandCollab = document.createElement('input');  
  expandCollab.type = "button";
  expandCollab.className = "option_text";
  expandCollab.id = "expand_col_button";
  expandCollab.addEventListener('click',toggleCommentLink);
  if (localStorage.getItem(commentPermaLinkStorageTag) == 1){
	  expandCollab.value ="Do Not Include Comment Links";
	  expandCollab.classList.add("selected");
  }
  else{
	  expandCollab.value ="Include Comment Links";
  }
  optionsbar.appendChild(expandCollab);
  var breaker = document.createElement('br');
  optionsbar.appendChild(breaker);
  //set time zone
  var timeZonelab = document.createElement('input');
  timeZonelab.type = "button";
  timeZonelab.className = "option_text";
  timeZonelab.id = "time_zone_button";
  timeZonelab.addEventListener('click',toggleTimeZone);
  if (localStorage.getItem(timeZoneStorageTag) == 1){
    timeZonelab.value = "Set Server Time Zone";
	  timeZonelab.classList.add("selected");
  }
  else{
    timeZonelab.value = "Set Local Time Zone";
  }    
  optionsbar.appendChild(timeZonelab);
  var breaker2 = document.createElement('br');
  optionsbar.appendChild(breaker2);
  //dynamically load comments
  var dynamicLoadlab = document.createElement('input');  
  dynamicLoadlab.type = "button";
  dynamicLoadlab.className = "option_text";
  dynamicLoadlab.id = "dynamic_load_button";
  dynamicLoadlab.addEventListener('click',toggleDynamicLoad);
  if (localStorage.getItem(dynamicLoadStorageTag) == 1){
    dynamicLoadlab.value = "Don't load Comments";
	  dynamicLoadlab.classList.add("selected");
    dynamicLoadInit();
  }
  else{
    dynamicLoadlab.value = "Dynamically Load Comments";    
  }
  optionsbar.appendChild(dynamicLoadlab);
  var breaker5 = document.createElement('br');  
  optionsbar.appendChild(breaker5);
  //compact comments
  var compactlab = document.createElement('input');  
  compactlab.type = "button";
  compactlab.className = "option_text";
  compactlab.id = "compact_comment_button";
  compactlab.addEventListener('click',toggleCompactComment);
  if (localStorage.getItem(compactCommentStorageTag) == 1){
    compactlab.value = "No Compact Comments";
	  compactlab.classList.add("selected");
    $( ".comment" ).addClass("compact");
  }
  else{
    compactlab.value = "Compact Comments";    
  }
  optionsbar.appendChild(compactlab);
  var breaker6 = document.createElement('br');  
  optionsbar.appendChild(breaker6);
}

function toggleCommentLink(){
  if (localStorage.getItem(commentPermaLinkStorageTag) == 1){
    localStorage.setItem(commentPermaLinkStorageTag,0);  
    document.getElementById('expand_col_button').value = "Include Comment Links";
    document.getElementById('expand_col_button').classList.remove("selected");
    $( ".comment_link" ).remove();
  }  
  else{
    localStorage.setItem(commentLinkStorageTag,1);  
    document.getElementById('expand_col_button').value = "Do Not Include Comment Links";
    document.getElementById('expand_col_button').classList.add("selected");
    $( ".comment_postinfo" ).each(function(){addLink(this);});
  }
}

function toggleTimeZone(){
  if (localStorage.getItem(timeZoneStorageTag) == 1){
    localStorage.setItem(timeZoneStorageTag,0);  
    document.getElementById('time_zone_button').value = "Set Local Time Zone";
    document.getElementById('time_zone_button').classList.remove("selected");
  }
  else{
    localStorage.setItem(timeZoneStorageTag,1);  
    document.getElementById('time_zone_button').value = "Set Server Time Zone";
    document.getElementById('time_zone_button').classList.add("selected");
  }
}

function toggleDynamicLoad(){
  if (localStorage.getItem(dynamicLoadStorageTag) == 1){
    localStorage.setItem(dynamicLoadStorageTag,0);  
    document.getElementById('dynamic_load_button').value = "Dynamically load Comments";
    document.getElementById('dynamic_load_button').classList.remove("selected");
    dynamicLoadStop();
  }
  else{
    localStorage.setItem(dynamicLoadStorageTag,1);  
    document.getElementById('dynamic_load_button').value = "Don't load Comments";
    document.getElementById('dynamic_load_button').classList.add("selected");
    dynamicLoadInit();
  }
}

function dynamicLoadInit(){
  timer = setInterval(function(){dynamicLoadRefresh();},45000);
}

function dynamicLoadRefresh(){
  //TODO: preserve the current new comments
  localStorage.setItem("newComments", $(".new-comment").map(function(){return "#"+this.id;}).get().join(","));
  $("#comments").load(document.URL + " #comments");
  if ($("#respond").parent().hasClass("comment")){
    return;
    //localStorage.setItem("replyComment", "#"+$("#respond").parent().attr('id'));
    //$("#respond").appendTo("#comment-wrap");    
    //movedReply = 1;
  }
  $( ".commentlist" ).load(document.URL + " .commentlist > *", function(){
            /*if (movedReply > 0){
              var rep_str = localStorage.getItem("replyComment");
              if ($(rep_str).children(".children").length > 0){
                $("#respond").insertBefore(rep_str + " > .children");
              }
              else{
                $("#respond").appendTo(rep_str);
              }
              movedReply = 0;
            }*/
            var nid_str = localStorage.getItem("newComments");
            $(nid_str).addClass("new-comment");
            handleComments();     
            updateCompactComment();
            $("#unread_button").val(document.querySelectorAll(".new-comment").length + " Unread Comments");
            if (document.querySelectorAll(".new-comment").length == 1){
              $("#unread_button").val(document.querySelectorAll(".new-comment").length + " Unread Comment");
            }
          });
}

function dynamicLoadStop(){
  clearInterval(timer);
}

function toggleCompactComment(){
  if (localStorage.getItem(compactCommentStorageTag) == 1){
    localStorage.setItem(compactCommentStorageTag,0);  
    document.getElementById('compact_comment_button').value = "Compact Comments";
    document.getElementById('compact_comment_button').classList.remove("selected");
    $( ".comment" ).removeClass("compact");
  }
  else{
    localStorage.setItem(compactCommentStorageTag,1);  
    document.getElementById('compact_comment_button').value = "No Compact Comments";
    document.getElementById('compact_comment_button').classList.add("selected");
    $( ".comment" ).addClass("compact");
  }
}

function updateCompactComment(){
  if (localStorage.getItem(compactCommentStorageTag) == 1){
    $( ".comment" ).addClass("compact");
  }
}

function toggleOptionsBar(){
  var optionsbar = document.getElementById('options_menu');
  if (optionsbar.classList.contains("mini")){
    optionsbar.classList.remove("mini");
  }
  else{
    optionsbar.classList.add("mini");
  }
}

function customFormatter() {
    if ( document.getElementById('comment') ) {
        document.getElementById('comment').value="";
        var tags = [ "a=link", "strong=bold", "strike=strikethrough", "em=italics", "blockquote=blockquote"];
        var sfc = document.createElement("span");
        for ( tag in tags ) {
            var tagname=tags[tag].split('=')[0];
            var tagdesc=tags[tag].split('=')[1];
            var thisTag=document.createElement('button');
            thisTag.id=tagname + "-tag";
            thisTag.type = "button";
            thisTag.setAttribute('title', tagdesc + ' text');
            if ( tagname == "a" ) {
                thisTag.onclick = function(but){ 
                var form=but.target.parentElement.parentElement.parentElement;
                doAnchorDialog(form.getElementsByTagName('textarea')[0]); 
                return false; 
                };
            }
            else {
                thisTag.onclick = function(but) { 
                console.log("click event = ",but);
                var form=but.target.parentElement.parentElement.parentElement;
                myFormatText(but.target.id.replace(/-tag/,""),form.getElementsByTagName('textarea')[0]); 
                return false; 
                };
            }
            thisTag.innerHTML=tagdesc;
            sfc.appendChild(thisTag);
        }
        var sa=document.getElementById('comment');
        sa.parentNode.insertBefore(sfc, sa);
    }
}

function myFormatText(tag,ta) {
    var reptext="";
    var startPos = ta.selectionStart;
    var endPos = ta.selectionEnd;
    if ( tag == "a" ) {
        reptext="<a href=" + ta.value.substring(startPos, endPos) + 'target="_blank"' + ">" + ta.value.substring(startPos, endPos) + "</a>";
    }
    else if ( document.getSelection().anchorNode != null && ! document.getSelection().anchorNode.contains(ta) && document.getSelection().toString().length > 0 ) {
      console.log('select-o-matic charged!');
      var tmpdiv = document.createElement("div");
      var content=document.getElementsByClassName('post')[0];
      var comment=document.getElementById('comment-wrap');
      var cite=document.getElementsByClassName('fn')[0].innerText;
      var pre="";
      var post="";
      var citeTime="";
      if (comment.contains(document.getSelection().anchorNode)) {
          var li;
          var notli=document.getSelection().anchorNode;
          for (var a=0; a < 10; a++ ) {
              li=notli.parentElement;
              if (li.tagName == 'ARTICLE' ) {
                  break;
              }
              else {
                  notli=li;
              }
          }
          citeTime=li.getElementsByClassName('comment_date')[0].innerText.replace("on ","");
          cite=li.getElementsByClassName('fn')[0].innerText.replace(/<script(?:.|\s)*\/script>/m, "");
      }
      if ( tag == "cite" ) {
          pre = "<cite>" + cite + " said";
          if ( citeTime.length > 0 ) {
              pre += " @" + citeTime;
          }
          pre += ": </cite><blockquote><P>";
          post = "</P></blockquote>";
      }
      else {
          pre="<" + tag + ">";
          post="</" + tag + ">";
      }
      var sel = window.getSelection();
      for (var i = 0, len = sel.rangeCount; i < len; ++i) {
        tmpdiv.appendChild(sel.getRangeAt(i).cloneContents());
      }
      reptext = reptext.concat(tmpdiv.innerHTML).replace(/<!--[^>]*>/,"").replace(/<\/?div[^>]*>/,"").replace(/<span class="new-text">[^>]*>/,"").replace(/<\/?span[^>]*>/,"");
      reptext = pre + reptext + post;
    }
    else {
        reptext="<" + tag + ">" + ta.value.substring(startPos, endPos) + "</" + tag + ">";
    }
    replaceWhereWithWhat(ta,startPos,endPos,reptext);
}

function replaceWhereWithWhat(ta,startPos,endPos,reptext) {
    if (ta.value == "COMMENT"){
      killPreloadedCommentText();
    }
    ta.value=ta.value.substring(0,startPos) + reptext + ta.value.substring(endPos,ta.value.length);
}

function doAnchorDialog(ta) {
    ta=document.getElementsByTagName('textarea')[0];

    var startPos = ta.selectionStart;
    var endPos = ta.selectionEnd;
    
    mydiv=document.createElement('div');
    mydiv.setAttribute('role','dialog');
    mydiv.setAttribute('class','anchor-floater');
    mydiv.setAttribute('tabindex',0);
    mydiv.setAttribute('style','left: 480px; top: 324px;');
    mydiv.id='anchor-dialog';

    var mybut=document.createElement('button');
    mybut.id='anchor-closer';
    mybut.setAttribute('title', 'close it');
    mybut.onclick=function() { document.getElementById('anchor-dialog').remove(); return false; };
    mybut.innerHTML='X';
    mydiv.appendChild(mybut);

    var anchorName=document.createElement('input');
    anchorName.id='anchorName';
    anchorName.value=ta.value.substring(startPos, endPos);
    var anchorNameLab=document.createElement('label');
    anchorNameLab.innerHTML='Display Text';
    mydiv.appendChild(anchorNameLab);
    mydiv.appendChild(anchorName);

    var anchorUrl=document.createElement('input');
    anchorUrl.id='anchorUrl';
    anchorUrl.tabIndex=0;
    var tmpval=ta.value.substring(startPos, endPos);
    if ( tmpval.length > 0 && tmpval.substring(0,4) != "http" ) {
        tmpval="http://" + tmpval;
    }
    anchorUrl.value=tmpval;
    var anchorUrlLab = document.createElement('label');
    anchorUrlLab.innerHTML='Url';
    mydiv.appendChild(anchorUrlLab);
    mydiv.appendChild(anchorUrl);
    
    var titleName=document.createElement('input');
    titleName.id='titleName';
    titleName.value=ta.value.substring(startPos, endPos);
    var titleNameLab=document.createElement('label');
    titleNameLab.innerHTML='Hover Text';
    mydiv.appendChild(titleNameLab);
    mydiv.appendChild(titleName);

    var mysubbut=document.createElement('button');
    mysubbut.id='anchor-submit';
    mysubbut.onclick=function() { 
        var tmpval2=anchorUrl.value;
        if ( tmpval2.length > 0 && tmpval2.substring(0,4) != "http" ) {
            tmpval2="http://" + tmpval2;
        }
        reptext='<a href="' + tmpval2 + '" ';
        reptext += (titleName.value) ? 'title="' + titleName.value + '" ' : '';
        reptext += ' target="_blank" ';
        reptext += '>' + anchorName.value + '</a>';
        replaceWhereWithWhat(ta,startPos,endPos,reptext);
        document.getElementById('anchor-dialog').remove(); 
        var cc=ta.parentElement.getElementsByClassName('characterCount')[0];
        updateNumCharsInElement(ta.value,cc,maxCommentLength);
        return false; 
    };
    mysubbut.innerHTML='submit';
    mydiv.appendChild(mysubbut);

    ta.parentElement.appendChild(mydiv);
    anchorName.focus();
}

